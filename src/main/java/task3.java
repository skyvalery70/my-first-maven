
public class task3 {

    public static void main(String[] args) {
        int countVectorInArray = (int)(Math.random() * 8 + 2);
        Vector vector1 = new Vector(Math.random()*10, Math.random()*10, Math.random()*10 );
        Vector vector2 = new Vector(Math.random()*10, Math.random()*10, Math.random()*10 );
        System.out.println("Вектор 1 " + vector1.show());
        System.out.println("Вектор 2 " + vector2.show());
        System.out.println("Длина вектора 1 = " + vector1.length());
        System.out.println("Длина вектора 2 = " + vector2.length());
        System.out.println("Сумма векторов " + VectorOperation.sumVectors(vector1, vector2).show());
        System.out.println("Разность векторов " + VectorOperation.diffVectors(vector1, vector2).show());
        System.out.println("Скалярное произведение векторов " + VectorOperation.scalarVectors(vector1, vector2));
        System.out.println("Угол(косинус угла) между векторами " + VectorOperation.angleVectors(vector1, vector2));
        System.out.println("Векторное произведение " + VectorOperation.vectorVectors(vector1, vector2).show());
        Vector[] vectorsInArray = VectorOperation.randomVectors(countVectorInArray);
        for (int i = 0; i < countVectorInArray; i++) {
            System.out.println("Случайный вектор номер " + (i + 1) +  " из массива: " + vectorsInArray[i].show());
        }

    }

}

class Vector {
    private Double x, y, z;
    public Vector(Double x, Double y, Double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public String show() {
        return " (" +  x  + " " + y + " " + z + ")";
    }

    public double length() {
        return Math.sqrt(x*x + y*y + z*z);
    }

    public double getX() {
        return x;
    }

    private void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    private void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    private void setZ(double z) {
        this.z = z;
    }

}

class VectorOperation  {
    final Vector v1, v2;
    VectorOperation (Vector v1, Vector v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    static Vector sumVectors(Vector v1, Vector v2) {

        return new Vector(v1.getX() + v2.getX(), v1.getY() + v2.getY(), v1.getZ() + v2.getZ() );

     }

    static Vector diffVectors(Vector v1, Vector v2) {

        return new Vector(v1.getX() - v2.getX(), v1.getY() - v2.getY(), v1.getZ() - v2.getZ() );

    }

    static Vector vectorVectors(Vector v1, Vector v2) {

        return new Vector((v1.getY() * v2.getZ() - v1.getZ() * v2.getY()), (v1.getZ() * v2.getX() - v1.getX() * v2.getZ()), (v1.getX() * v2.getY() - v1.getY() * v2.getX()) );

    }

    static Double scalarVectors(Vector v1, Vector v2) {
        return v1.getX() * v2.getX() + v1.getY() * v2.getY() + v1.getZ() * v2.getZ();
    }

    static Double angleVectors(Vector v1, Vector v2) {

        return scalarVectors(v1, v2) / (v1.length() * v2.length());
    }

    static Vector[] randomVectors(Integer n) {
        Vector[] v = new Vector[n];
        for (int i = 0; i < n; i++) {
            v[i] = new Vector(Math.random()*10, Math.random()*10, Math.random()*10 );
        }
        return v;
    }

}


