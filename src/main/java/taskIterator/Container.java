package taskIterator;

import java.util.Iterator;

public interface Container {
    Iterator getIterator();
}
