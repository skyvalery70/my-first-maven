package taskIterator;

public interface myIterator {
    Object next();
    boolean hasNext();
    void remove();
}
