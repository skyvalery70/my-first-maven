package taskIterator;

import java.util.Iterator;

public class taskIterator {

    public static void main(String[] args) {

        ArrayContainer arrayContainer = new ArrayContainer();
        Iterator iterator = arrayContainer.getIterator();

        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }

}


class ArrayContainer implements Container {
    String[] array = {"first", "second", "third"};

    @Override
    public ArrayIterator getIterator() {
        return new ArrayIterator();
    }


    class ArrayIterator implements Iterator {
        int index;

        @Override
        public void remove() {


        }

        @Override
        public boolean hasNext() {
            return index < array.length;
        }

        @Override
        public Object next() {
            if(hasNext()) {
                return array[index++];
            }
            return null;
        }
    }
}


