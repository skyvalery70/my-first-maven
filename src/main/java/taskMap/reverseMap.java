package taskMap;

import java.util.*;

class reverseMap {

    static HashMap<String, ArrayList<String>> changeKeyValue (HashMap<String, String> userMap)
    {
        HashMap<String, ArrayList<String>> newMap = new HashMap<>();

        System.out.println(userMap.values());
        System.out.println(userMap.keySet());

        for(Map.Entry<String, String> entry : userMap.entrySet()) {

            var tempValue = new ArrayList<String>();
            tempValue.add(entry.getKey());

            if (newMap.containsKey(entry.getValue())) {
                newMap.get(entry.getValue()).add(entry.getKey());
            } else {
                newMap.put(entry.getValue(), tempValue);
            }

        }

        return newMap;
    }

}