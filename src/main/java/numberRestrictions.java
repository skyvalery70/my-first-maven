import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

public class numberRestrictions {

    public static LocalDate formatDate(String dateInString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        return LocalDate.parse(dateInString, formatter);
    }

    public static void counterRestrictions(String dateToCheck, String pathToFile) throws IOException {
        int counter = 0;
        List<String> lines = Files.readAllLines(Path.of(pathToFile));
        try {
            for (String line : lines) {
                String[] currentLine = line.split(",");
                if (isBetween(formatDate(currentLine[10]), formatDate(currentLine[11]), formatDate(dateToCheck))) {
                    counter += 1;
                }
            }
        } catch (DateTimeParseException e) {
            
//            for (String line : lines) {
//                String[] currentLine = line.split(",");
//                if (isBetween(formatDate(currentLine[11]), formatDate(currentLine[12]), formatDate(dateToCheck))) {
//                    counter += 1;
//                }
//            }
        }
        System.out.println("Количество ограничений на дату  " + formatDate(dateToCheck) + " равно " + counter);


    }

    public static Boolean isBetween(LocalDate startDate, LocalDate finishDate, LocalDate dateToCompare) {
        return (dateToCompare.isAfter(startDate) || dateToCompare.isEqual(startDate)) && (dateToCompare.isBefore(finishDate) || dateToCompare.isEqual(finishDate));
    }

    public static void main(String[] args) throws IOException {
//        try {
            counterRestrictions("20160212", "C:\\Users\\Валерий\\IdeaProjects\\Lessons\\test.csv");

//        } catch(DateTimeParseException e){
//            System.out.println("Дату надо в формате yyyyMMdd");
//        }
    }
}


